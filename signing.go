package auth

import (
	"log"
	"time"

	"github.com/dgrijalva/jwt-go"
)

//CreateRsaToken create token
func CreateRsaToken(priveKeyByte []byte, payload map[string]interface{}) (string, error) {
	if priveKeyByte == nil {
		log.Fatal("no private key")
	}

	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(priveKeyByte)
	if err != nil {
		log.Fatal(err)
	}

	// create a signer for rsa 256
	t := jwt.New(jwt.GetSigningMethod("RS256"))

	//set header
	t.Header["kid"] = "PollyNarakAndBeautyMakMakKa"

	claims := jwt.MapClaims{
		"exp": time.Now().Add(time.Minute * 60 * 25).Unix(),
	}
	for k, v := range payload {
		claims[k] = v
	}
	t.Claims = claims

	log.Println(t.Claims)
	// Creat token string
	return t.SignedString(signKey)
}
