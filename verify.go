package auth

import (
	"log"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

//VerifyRsaToken Verify token and parse data
func VerifyRsaToken(pubkeyBytes []byte, tokenString string) (jwt.MapClaims, bool) {
	if tokenString == "" {
		log.Println("empty token")
		return nil, false
	}
	tokenString = strings.TrimSpace(tokenString)
	verifyKey, err := jwt.ParseRSAPublicKeyFromPEM(pubkeyBytes)
	if err != nil {
		log.Println("verifypub Key")
		return nil, false
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return verifyKey, nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, true
	} else {
		log.Println("Invalid JWT Token")
		return nil, false
	}
}
